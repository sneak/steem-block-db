package main

import log "github.com/sirupsen/logrus"

// wtf go stdlib doesn't have a Logger interface?!
type Logger interface {
	log.FieldLogger
}
