package main

import "testing"
import log "github.com/sirupsen/logrus"

/*
import "github.com/stretchr/testify/mock"

type MockedSteemAPI struct {
	mock.Mock
}

func (m *MockedSteemAPI) DoSomething(number int) (bool, error) {
	args := m.Called(number)
	return args.Bool(0), args.Error(1)
}

*/

func TestBlockfetcherInit(t *testing.T) {

	log.SetLevel(log.DebugLevel)

	bf := NewBlockFetcher(&BlockFetcherConfig{
		api:                   nil,
		desiredFetcherThreads: 1,
		startBlock:            10000,
		endBlock:              10005,
	})

	if bf == nil {
		t.Errorf("could not instantiate blockfetcher")
	}

}

//can't actually fetch yet until we mock the api
/*
func TestBlockfetcherFetch(t *testing.T) {

	log.SetLevel(log.DebugLevel)

	bf := NewBlockFetcher(&BlockFetcherConfig{
		api:                   nil,
		desiredFetcherThreads: 1,
		startBlock:            10000,
		endBlock:              10005,
	})

	bf.fetch()
}

*/
