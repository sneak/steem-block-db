package main

import "encoding/json"

//import log "github.com/sirupsen/logrus"

type SteemAPI struct {
	url string
	rpc *JSONRPC
}

type SteemAPIShape interface {
	GetDynamicGlobalProperties() (GetDynamicGlobalPropertiesResponse, error)
	GetOpsInBlock(blockNum BlockNumber) (GetOpsInBlockResponse, error)
}

var EmptyParams = []string{}
var EmptyParamsRaw, _ = json.Marshal(EmptyParams)

func NewSteemAPI(url string, options ...func(s SteemAPIShape)) *SteemAPI {

	rpc := NewJSONRPC(url, func(x *JSONRPC) {})

	self := &SteemAPI{
		rpc: rpc,
	}

	for _, option := range options {
		option(self)
	}

	return self
}

func (self *SteemAPI) GetDynamicGlobalProperties() (GetDynamicGlobalPropertiesResponse, error) {
	var resp DynamicGlobalProperties
	raw, err := self.rpc.Call("condenser_api.get_dynamic_global_properties", EmptyParamsRaw)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(raw, &resp)
	return &resp, nil
}

func (self *SteemAPI) GetOpsInBlock(blockNum BlockNumber) (GetOpsInBlockResponse, error) {
	// i was mistaken, i thought the second param == true meant only
	// virtualops, and == false meant only non-virtualops.  turns out the
	// arg should be named "excludenonvirtualops", as setting it to false
	// returns both real ops *and* virtual ops in a single call. not sure if
	// this was always the case, but it is as of 20181101 against
	// api.steemit.com.
	realOpsParams := &GetOpsInBlockRequestParams{BlockNum: blockNum, VirtualOps: false}
	rop, err := realOpsParams.MarshalJSON()
	if err != nil {
		return nil, err
	}
	rawOpsResponse, err := self.rpc.Call("condenser_api.get_ops_in_block", rop)
	var result []OperationObject
	err = json.Unmarshal(rawOpsResponse, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
