GOFILES := $(shell find . -type f -name '*.go' -not -name '*_test.go')

default: test

.PHONY: run build test

run:
	go run $(GOFILES)

build: steem-block-db

steem-block-db: *.go
	go build

test:
	go test

clean:
	rm steem-block-db
