package main

import "encoding/json"
import "github.com/joeshaw/iso8601"

type BlockNumber uint64

type OperationObject struct {
	BlockNumber        BlockNumber       `json:"block"`
	OpInTx             int               `json:"op_in_trx"`
	Operation          []json.RawMessage `json:"op"`
	Timestamp          iso8601.Time      `json:"timestamp"`
	TransactionID      string            `json:"trx_id"`
	TransactionInBlock BlockNumber       `json:"trx_in_block"`
	VirtualOperation   int               `json:"virtual_op"`
}

type GetOpsInBlockRequestParams struct {
	BlockNum   BlockNumber
	VirtualOps bool
}

func (r *GetOpsInBlockRequestParams) MarshalJSON() ([]byte, error) {
	arr := []interface{}{r.BlockNum, r.VirtualOps}
	return json.Marshal(arr)
}

type DynamicGlobalProperties struct {
	ConfidentialSbdSupply        string      `json:"confidential_sbd_supply"`
	ConfidentialSupply           string      `json:"confidential_supply"`
	CurrentAslot                 int         `json:"current_aslot"`
	CurrentSbdSupply             string      `json:"current_sbd_supply"`
	CurrentSupply                string      `json:"current_supply"`
	CurrentWitness               string      `json:"current_witness"`
	DelegationReturnPeriod       int         `json:"delegation_return_period"`
	HeadBlockID                  string      `json:"head_block_id"`
	HeadBlockNumber              int         `json:"head_block_number"`
	LastIrreversibleBlockNum     BlockNumber `json:"last_irreversible_block_num"`
	MaximumBlockSize             int         `json:"maximum_block_size"`
	NumPowWitnesses              int         `json:"num_pow_witnesses"`
	ParticipationCount           int         `json:"participation_count"`
	PendingRewardedVestingShares string      `json:"pending_rewarded_vesting_shares"`
	PendingRewardedVestingSteem  string      `json:"pending_rewarded_vesting_steem"`
	RecentSlotsFilled            string      `json:"recent_slots_filled"`
	ReverseAuctionSeconds        int         `json:"reverse_auction_seconds"`
	SbdInterestRate              int         `json:"sbd_interest_rate"`
	SbdPrintRate                 int         `json:"sbd_print_rate"`
	SbdStartPercent              int         `json:"sbd_start_percent"`
	SbdStopPercent               int         `json:"sbd_stop_percent"`
	Time                         string      `json:"time"`
	TotalPow                     int         `json:"total_pow"`
	TotalRewardFundSteem         string      `json:"total_reward_fund_steem"`
	TotalRewardShares2           string      `json:"total_reward_shares2"`
	TotalVestingFundSteem        string      `json:"total_vesting_fund_steem"`
	TotalVestingShares           string      `json:"total_vesting_shares"`
	VirtualSupply                string      `json:"virtual_supply"`
	VotePowerReserveRate         int         `json:"vote_power_reserve_rate"`
}

type GetOpsInBlockResponse *[]OperationObject

type GetDynamicGlobalPropertiesResponse *DynamicGlobalProperties
