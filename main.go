package main

import "github.com/spf13/viper"
import log "github.com/sirupsen/logrus"

// STEEM_APIURL=https://api.steem.house ./steem-block-db

func main() {
	viper.SetConfigName("steem")
	viper.AddConfigPath("/etc/steem")
	viper.AddConfigPath("$HOME/.config/steem")
	viper.SetEnvPrefix("steem")
	viper.BindEnv("debug")
	viper.BindEnv("redis")
	viper.BindEnv("apiurl")
	viper.ReadInConfig() // Find and read the config file if exists
	logLevel := log.InfoLevel
	if viper.GetBool("debug") == true {
		logLevel = log.DebugLevel
	}
	redis := "localhost:6379"
	if viper.Get("redis") != nil {
		redis = viper.GetString("redis")
	}
	apiurl := "https://api.steemit.com"
	if viper.Get("apiurl") != nil {
		apiurl = viper.GetString("apiurl")
	}
	app := NewApp(&appconfig{
		logLevel:              logLevel,
		apiUrl:                apiurl,
		redisUrl:              redis,
		desiredFetcherThreads: 30,
	})
	app.main()
}
